# setel
Order: https://0z24hy4j0i.execute-api.ap-southeast-1.amazonaws.com/staging

Payment: https://9abm35wrxj.execute-api.ap-southeast-1.amazonaws.com/staging

---
1] Calling Orders App endpoint to create an Order

[POST] https://0z24hy4j0i.execute-api.ap-southeast-1.amazonaws.com/staging/orders

Body:
```json
{
    "total": 20000,
    "placedItems": [
        {
            "name": "motor",
            "price": 2000
        },
        {
            "name": "car",
            "price": 18000
        }
    ]
}
```

2] An order should be created in DB with the created state. ~ the api call will return persisted order with its generated id.

Result:
```json
{
    "placedItems": [
        {
            "name": "motor",
            "price": 2000,
            "id": 59
        },
        {
            "name": "car",
            "price": 18000,
            "id": 60
        }
    ],
    "total": 20000,
    "status": "CREATED",
    "id": 38
}
```

3] Orders App makes a call to Payment App with the order information and mocked auth details.
4] Payment App processes an order (logic can be mocked) and returns random response confirmed or declined


[POST] https://0z24hy4j0i.execute-api.ap-southeast-1.amazonaws.com/staging/orders/{id}/payment

Body:
```json
{
    "eWalletId": "setel",
    "pin": "setel123"
}
```

5] Orders App updates order based on the response from the Payments App
* declined ⇒ moves order to the canceled state
* confirmed ⇒ moves order to the confirmed state

[GET] https://0z24hy4j0i.execute-api.ap-southeast-1.amazonaws.com/staging/orders/{id}

Result:
```json
{
    "id": 38,
    "total": 20000,
    "status": "CONFIRMED"
}
```

6] After X amount of seconds confirmed orders should automatically be moved to the delivered state.

Wait for 10 seconds and check order status again

[GET] https://0z24hy4j0i.execute-api.ap-southeast-1.amazonaws.com/staging/orders/{id}/status

Result:
```
DELIVERED
```
