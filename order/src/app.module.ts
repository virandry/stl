import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { OrdersModule } from './orders/orders.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'rosie.db.elephantsql.com',
      port: 5432,
      username: 'bcodhckt',
      password: 'e6bNmLPL9cqAMcaT0ho2g2ctlhlx0wki',
      database: 'bcodhckt',
      synchronize: true,
      autoLoadEntities: true,
    }),
    OrdersModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private readonly connection: Connection) {}
}
