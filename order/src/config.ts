export const configs = {
  serverless: {
    order: 'https://0z24hy4j0i.execute-api.ap-southeast-1.amazonaws.com/staging',
    payment: 'https://9abm35wrxj.execute-api.ap-southeast-1.amazonaws.com/staging',
  },
  local: {
    order: 'http://localhost:3000',
    payment: 'http://localhost:3001',
  },
};
