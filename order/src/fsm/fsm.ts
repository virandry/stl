import * as StateMachine from 'javascript-state-machine';
import { Status } from './status';
import { getConnection } from 'typeorm';
import { OrderEntity } from '../orders/orders.entity';

async function updateStatus(orderId, status) {
  const repo = await getConnection().getRepository(OrderEntity);
  const toUpdate = await repo.findOne(orderId);
  const updated = Object.assign(toUpdate, { status });
  console.log('updated: ', updated);
  return await repo.save(updated);
}

export class Machine {
  fsm: any;
  constructor(init) {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const self = this;
    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
    // @ts-ignore
    this.fsm = new StateMachine({
      init,
      transitions: [
        { name: 'confirm', from: Status.CREATED, to: Status.CONFIRMED },
        { name: 'decline', from: Status.CREATED, to: Status.CANCELLED },
        { name: 'cancel', from: Status.CONFIRMED, to: Status.CANCELLED },
        { name: 'deliver', from: Status.CONFIRMED, to: Status.DELIVERED },
      ],
      methods: {
        onConfirm: async function(_, orderId) {
          const newStatus = await updateStatus(orderId, Status.CONFIRMED);
          setTimeout(() => self.deliver(orderId));
          return newStatus;
        },
        onDecline: async function(_, orderId) {
          return await updateStatus(orderId, Status.CANCELLED);
        },
        onCancel: async function(_, orderId) {
          return await updateStatus(orderId, Status.CANCELLED);
        },
        onDeliver: async function(_, orderId) {
          return await updateStatus(orderId, Status.DELIVERED);
        },
      },
    });
  }

  async deliver(orderId: number) {
    // After X amount of seconds confirmed orders should automatically be moved to the delivered state.
    setTimeout(async () => {
      const repo = await getConnection().getRepository(OrderEntity);
      const order = await repo.findOne(orderId);
      if (order.status === Status.CONFIRMED) await this.fsm.deliver(order.id);
    }, 10000);
  }
}
