import { Module, HttpModule } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { OrdersController } from './orders.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PlacedItemEntity } from './placed-items.entity';
import { OrderEntity } from './orders.entity';

@Module({
  imports: [HttpModule, TypeOrmModule.forFeature([OrderEntity, PlacedItemEntity])],
  providers: [OrdersService],
  controllers: [OrdersController],
})
export class OrdersModule {}
