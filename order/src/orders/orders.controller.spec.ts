/* eslint-disable @typescript-eslint/ban-ts-ignore */
import { Test, TestingModule } from '@nestjs/testing';
import { OrdersController } from './orders.controller';
import { OrdersService } from './orders.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { OrderEntity } from './orders.entity';
import { Repository } from 'typeorm';
import { HttpModule } from '@nestjs/common';
import { Status } from '../fsm/status';

describe('Orders Controller', () => {
  let ordersController: OrdersController;
  let ordersService: OrdersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [
        OrdersService,
        {
          provide: getRepositoryToken(OrderEntity),
          useClass: Repository,
        },
      ],
      controllers: [OrdersController],
    }).compile();

    ordersService = module.get<OrdersService>(OrdersService);
    ordersController = module.get<OrdersController>(OrdersController);
  });

  it('should be defined', () => {
    expect(ordersService).toBeDefined();
    expect(ordersController).toBeDefined();
  });

  describe('order getter', () => {
    it('/GET orders', () => {
      const result = [
        {
          id: 1,
          total: 200,
          status: 'CREATED',
        },
        {
          id: 2,
          total: 200,
          status: 'CREATED',
        },
        {
          id: 3,
          total: 200,
          status: 'DELIVERED',
        },
      ];
      // @ts-ignore
      jest.spyOn(ordersService, 'findAll').mockImplementation(() => result);
      expect(ordersController.getOrders()).toBe(result);
    });

    it('/GET order with id', () => {
      const result = {
        id: 3,
        total: 200,
        status: 'DELIVERED',
      };
      // @ts-ignore
      jest.spyOn(ordersService, 'findOne').mockImplementation(() => result);
      expect(ordersController.getOrderById(3)).toBe(result);
    });

    it('can check order status', async () => {
      const order = {
        id: 3,
        total: 200,
        status: 'DELIVERED',
      };
      // @ts-ignore
      jest.spyOn(ordersService, 'findOne').mockImplementation(() => order);
      const result = order.status;
      expect(await ordersController.getOrderStatusById(3)).toBe(result);
    });
  });

  describe('order lifecycle', () => {
    it('creates order', async () => {
      const result = {
        placedItems: [
          {
            name: 'motor',
            price: 2000,
            id: 63,
          },
          {
            name: 'car',
            price: 18000,
            id: 64,
          },
        ],
        total: 20000,
        status: 'CREATED',
        id: 40,
      };
      // @ts-ignore
      jest.spyOn(ordersService, 'create').mockImplementation(() => result);
      const orderDto = {
        total: 20000,
        placedItems: [
          {
            name: 'motor',
            price: 2000,
          },
          {
            name: 'car',
            price: 18000,
          },
        ],
      };
      // @ts-ignore
      expect(await ordersController.createOrder(orderDto)).toBe(result);
    });
  });
  it('perform payment for order [CREATED => CONFIRMED]', async () => {
    const order = {
      id: 3,
      total: 200,
      status: 'CREATED',
    };
    // @ts-ignore
    jest.spyOn(ordersService, 'findOne').mockImplementation(() => order);
    const result = Status.CONFIRMED;
    // @ts-ignore
    jest.spyOn(ordersService, 'confirmAndPay').mockImplementation(() => result);
    expect(await ordersController.confirmAndPay(3, { eWalletId: 'setel', pin: 'setel123' })).toBe(result);
  });

  it('perform payment for order [CREATED => CANCELLED]', async () => {
    const order = {
      id: 3,
      total: 200,
      status: 'CREATED',
    };
    // @ts-ignore
    jest.spyOn(ordersService, 'findOne').mockImplementation(() => order);
    const result = Status.CANCELLED;
    // @ts-ignore
    jest.spyOn(ordersService, 'confirmAndPay').mockImplementation(() => result);
    expect(await ordersController.confirmAndPay(3, { eWalletId: 'setel', pin: 'setel123' })).toBe(result);
  });

  it('can cancel order [CONFIRMED => CANCELLED]', async () => {
    const order = {
      id: 1,
      total: 200,
      status: 'CONFIRMED',
    };
    // @ts-ignore
    jest.spyOn(ordersService, 'findOne').mockImplementation(() => order);
    // @ts-ignore
    jest.spyOn(ordersService, 'cancel').mockImplementation(() => Status.CANCELLED);
    const result = Status.CANCELLED;
    expect(await ordersController.cancelOrder(1)).toBe(result);
  });
});
