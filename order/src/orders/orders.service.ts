import { HttpException, HttpService, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { validate } from 'class-validator';
import { OrderEntity } from './orders.entity';
import { Order } from './orders.interface';
import { CreateOrderDto, PaymentDto, UpdateOrderDto } from './orders.dto';
import { Status } from '../fsm/status';
import { Machine } from '../fsm/fsm';
import { configs } from '../config';

let depEnv = 'local';
console.log(process.env.DEP_ENV);
if (process.env.DEP_ENV) depEnv = process.env.DEP_ENV;
const config = configs[depEnv];
console.log(config);

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(OrderEntity)
    private readonly ordersRepository: Repository<OrderEntity>,
    private readonly httpService: HttpService,
  ) {}

  async findAll(): Promise<OrderEntity[]> {
    return await this.ordersRepository.find();
  }

  async findOne(id: number): Promise<Order> {
    return await this.ordersRepository.findOne(id);
  }

  async create(dto: CreateOrderDto): Promise<Order> {
    const { placedItems, total } = dto;
    console.log('dto: ', dto);

    // create new menu
    const newOrder = new OrderEntity();
    newOrder.placedItems = placedItems;
    newOrder.total = total;
    newOrder.status = Status.CREATED;

    console.log(newOrder.placedItems.length);

    const errors = await validate(newOrder);
    const isEmpty = newOrder.placedItems?.length < 1;
    if (errors.length > 0 || isEmpty) {
      const _errors = 'Order input is not valid.';
      throw new HttpException({ message: 'Input data validation failed', _errors }, HttpStatus.BAD_REQUEST);
    } else {
      const savedOrder = await this.ordersRepository.save(newOrder);
      console.log('savedOrder', savedOrder);
      return savedOrder;
    }
  }

  async confirmAndPay(order: Order, paymentDto: PaymentDto) {
    const { fsm } = new Machine(order.status);
    console.log('order.status: ', order.status);
    if (order.status === Status.CREATED) {
      const paymentResponse = await this.httpService
        .post(config.payment, {
          order,
          ...paymentDto,
        })
        .toPromise()
        .then(response => response.data);
      console.log('paymentResponse: ', paymentResponse);
      if (paymentResponse === 'confirmed') fsm.confirm(order.id);
      else fsm.decline(order.id);
      return fsm.state;
    }
    throw new HttpException(
      { message: 'Payment is not available for this Status: ' + order?.status },
      HttpStatus.INTERNAL_SERVER_ERROR,
    );
  }

  async cancel(order: Order) {
    const { fsm } = new Machine(order.status);
    if (order?.status === Status.CONFIRMED) {
      await fsm.cancel(order.id);
      return fsm.state;
    }
    throw new HttpException(
      { message: 'Cancellation is not available for this Status: ' + order?.status },
      HttpStatus.INTERNAL_SERVER_ERROR,
    );
  }
}
