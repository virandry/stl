import { IsNotEmpty } from 'class-validator';
import { PlacedItemEntity } from './placed-items.entity';
import { Status } from '../fsm/status';

export class CreateOrderDto {
  @IsNotEmpty()
  placedItems: PlacedItemEntity[];

  @IsNotEmpty()
  total: number;
}

export class UpdateOrderDto {
  readonly placedItems: PlacedItemEntity[];
  readonly total: number;
  readonly status: Status;
}

export class PaymentDto {
  readonly eWalletId;
  readonly pin;
}
