import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { OrderEntity } from './orders.entity';

@Entity('placed_item')
export class PlacedItemEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @ManyToOne(
    () => OrderEntity,
    OrderEntity => OrderEntity.placedItems,
  )
  order: OrderEntity;
}
