import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { CreateOrderDto, PaymentDto, UpdateOrderDto } from './orders.dto';
import { Status } from '../fsm/status';

@Controller('orders')
export class OrdersController {
  constructor(private ordersService: OrdersService) {}

  @Get()
  getOrders() {
    return this.ordersService.findAll();
  }

  @Get(':id')
  getOrderById(@Param() params) {
    console.log('get a single product', params.id);
    return this.ordersService.findOne(params.id);
  }

  @Get(':id/status')
  async getOrderStatusById(@Param() params) {
    console.log('get a single product', params.id);
    return (await this.ordersService.findOne(params.id)).status;
  }

  @Post()
  async createOrder(@Body() orderDto: CreateOrderDto) {
    return await this.ordersService.create(orderDto);
  }

  @Post(':id/payment')
  async confirmAndPay(@Param('id') orderId: number, @Body() paymentDto: PaymentDto) {
    const order = await this.ordersService.findOne(orderId);
    return await this.ordersService.confirmAndPay(order, paymentDto);
  }

  // order can only be cancelled only within 10s after the order has been confirmed
  @Post(':id/cancel')
  async cancelOrder(@Param('id') orderId: number) {
    const order = await this.ordersService.findOne(orderId);
    return await this.ordersService.cancel(order);
  }
}
