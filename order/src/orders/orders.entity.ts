import { Entity, PrimaryGeneratedColumn, OneToMany, Column } from 'typeorm';
import { PlacedItemEntity } from './placed-items.entity';
import { Status } from '../fsm/status';

@Entity('order')
export class OrderEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToMany(
    () => PlacedItemEntity,
    PlacedItemEntity => PlacedItemEntity.order,
    { cascade: true },
  )
  placedItems: PlacedItemEntity[];

  @Column()
  total: number;

  @Column()
  status: Status;
}
