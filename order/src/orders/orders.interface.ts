import { PlacedItemEntity } from './placed-items.entity';
import { Status } from '../fsm/status';

export interface Order {
  id: number;
  placedItems: PlacedItemEntity[];
  total: number;
  status: Status;
}
