export interface PaymentDto {
  readonly eWalletId;
  readonly pin;
  readonly order;
}
