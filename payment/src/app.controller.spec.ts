import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PaymentDto } from './app.dto';

expect.extend({
  toBeOneOf(received: any, items: Array<any>) {
    const pass = items.includes(received);
    const message = () => `expected ${received} to be contained in array [${items}]`;
    if (pass) {
      return {
        message,
        pass: true,
      };
    }
    return {
      message,
      pass: false,
    };
  },
});

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace jest {
    interface Matchers<R> {
      toBeOneOf(items: Array<any>): CustomMatcherResult;
    }
  }
}

describe('PaymentController', () => {
  let appController: AppController;
  let appService: AppService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    appService = app.get<AppService>(AppService);
    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "Hello Setel!"', () => {
      expect(appController.getHello()).toBe('Hello Setel!');
    });
  });

  describe('processPayment', () => {
    it('should return "declined" result if eWalletId and pin inputs are invalid.', () => {
      const result = 'declined';
      jest.spyOn(appService, 'verifyPaymentInfo').mockImplementation(() => false);
      const paymentDto: PaymentDto = {
        eWalletId: 'setel',
        pin: 'setel532',
        order: {},
      };
      expect(appController.processPayment(paymentDto)).toBe(result);
    });

    it('should return "confirmed" or "declined" result if eWalletId and pin inputs are valid.', () => {
      const result = ['confirmed', 'declined'];
      jest.spyOn(appService, 'verifyPaymentInfo').mockImplementation(() => true);
      const paymentDto: PaymentDto = {
        eWalletId: 'setel',
        pin: 'setel123',
        order: {
          placedItems: [
            {
              name: 'motor',
              price: 2000,
              id: 57,
            },
            {
              name: 'car',
              price: 18000,
              id: 58,
            },
          ],
          total: 200,
          status: 'CREATED',
          id: 37,
        },
      };
      expect(appController.processPayment(paymentDto)).toBeOneOf(result);
    });
  });
});
