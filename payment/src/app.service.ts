import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello Setel!';
  }

  verifyPaymentInfo(eWalletId, pin): boolean {
    return !(eWalletId !== 'setel' && pin !== 'setel123');
  }
}
