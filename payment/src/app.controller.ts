import { Body, Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { PaymentDto } from './app.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post()
  processPayment(@Body() paymentDto: PaymentDto): string {
    const { eWalletId, pin, order } = paymentDto;
    const resultSet = ['confirmed', 'declined'];

    const verification = this.appService.verifyPaymentInfo(eWalletId, pin);
    if (!verification) return resultSet[1];

    console.log('processing payment for order id:', order.id);
    return resultSet[Date.now() % 2];
  }
}
